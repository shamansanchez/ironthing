package net.jasonstone.ironthing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import io.prometheus.client.Gauge;
import io.prometheus.client.exporter.HTTPServer;

public final class IronThing extends JavaPlugin implements Listener {
	public static Logger logger;
	public DBStuff db;

	private HTTPServer server;

	static final Gauge itemCount = Gauge.build().name("barrel_itemcount").help("Item Count")
			.labelNames("barrel", "item").register();

	@Override
	public void onEnable() {
		logger = getLogger();

		try {
			server = new HTTPServer(18954);
		} catch (IOException e) {
			logger.warning(e.getLocalizedMessage());
		}

		getServer().getPluginManager().registerEvents(this, this);

		db = new DBStuff(this);
		logger.info(db.toString());

		PluginCommand createCommand = getCommand("monitorbarrel");
		CreateCommand createExecutor = new CreateCommand(this);
		createCommand.setExecutor(createExecutor);

		PluginCommand deleteCommand = getCommand("unmonitorbarrel");
		DeleteCommand deleteExecutor = new DeleteCommand(this);
		deleteCommand.setExecutor(deleteExecutor);

		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new BarrelTask(this), 0, 500);
	}
}

class BarrelTask implements Runnable {

	private final IronThing plugin;

	public BarrelTask(IronThing plugin) {
		this.plugin = plugin;
	}

	public void run() {
		// plugin.logger.info("waaaat");

		ArrayList<MonitoredBarrel> barrels = plugin.db.getMonitoredBarrels();

		IronThing.itemCount.clear();
		for (MonitoredBarrel barrel : barrels) {
			barrel.ding(IronThing.itemCount);
		}
	}
}
