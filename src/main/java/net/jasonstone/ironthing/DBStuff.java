package net.jasonstone.ironthing;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.bukkit.block.Block;

public class DBStuff {

  private IronThing plugin;

  public DBStuff(IronThing plugin) {
    this.plugin = plugin;

    try (Connection connection = _getConnection()) {
      String query = "CREATE TABLE IF NOT EXISTS barrels(name STRING PRIMARY KEY, world STRING NOT NULL, x INTEGER NOT NULL, y INTEGER NOT NULL, z INTEGER NOT NULL, UNIQUE(world,x,y,z));";
      Statement statement = connection.createStatement();
      statement.executeUpdate(query);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  private Connection _getConnection() throws SQLException {
    Connection connection = DriverManager
        .getConnection("jdbc:sqlite:" + plugin.getDataFolder() + File.separator + "ironthing.db");
    return connection;
  }

  public MonitoredBarrel addMonitoredBarrel(String name, String world, int x, int y, int z) {
    PreparedStatement statement;
    try (Connection connection = _getConnection()) {
      statement = connection.prepareStatement("INSERT INTO barrels VALUES(?, ?, ?, ?, ?)");
      statement.setString(1, name);
      statement.setString(2, world);

      statement.setInt(3, x);
      statement.setInt(4, y);
      statement.setInt(5, z);

      statement.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }

    return new MonitoredBarrel(name, world, x, y, z);
  }

  public boolean deleteMonitoredBarrel(String name) {
    PreparedStatement statement;
    try (Connection connection = _getConnection()) {

      statement = connection.prepareStatement("DELETE FROM barrels WHERE name=?");
      statement.setString(1, name);

      statement.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
      return false;
    }

    return true;
  }

  public ArrayList<MonitoredBarrel> getMonitoredBarrels() {
    ArrayList<MonitoredBarrel> ret = new ArrayList<MonitoredBarrel>();

    Statement statement;
    try (Connection connection = _getConnection()) {
      statement = connection.createStatement();
      ResultSet result = statement.executeQuery("SELECT * FROM barrels");
      while (result.next()) {
        ret.add(new MonitoredBarrel(result.getString("name"), result.getString("world"), result.getInt("x"),
            result.getInt("y"), result.getInt("z")));
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }

    return ret;
  }

  public MonitoredBarrel getMonitoredBarrelByBlock(Block block) {
    PreparedStatement statement;
    try (Connection connection = _getConnection()) {
      statement = connection.prepareStatement("SELECT * FROM barrels WHERE world=? AND x=? AND y=? AND z=?");
      statement.setString(1, block.getWorld().getName());
      statement.setInt(2, block.getX());
      statement.setInt(3, block.getY());
      statement.setInt(4, block.getZ());

      ResultSet result = statement.executeQuery();
      if (!result.next()) {
        return null;
      }

      return new MonitoredBarrel(result.getString("name"), result.getString("world"), result.getInt("x"),
          result.getInt("y"), result.getInt("z"));
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

}
