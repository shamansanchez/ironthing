package net.jasonstone.ironthing;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DeleteCommand implements CommandExecutor {
  private final IronThing plugin;

  public DeleteCommand(IronThing t) {
    plugin = t;
  }

  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (!(sender instanceof Player)) {
      return false;
    }

    Player player = (Player) sender;
    Block block = player.getTargetBlockExact(10);
    if (block == null) {
      return false;
    }

    MonitoredBarrel deadBarrel = plugin.db.getMonitoredBarrelByBlock(block);

    if (deadBarrel == null) {
      player.sendMessage(ChatColor.RED + "That's not a monitored barrel. Way to go.");
      return true;
    }

    deadBarrel.deleteEffect();

    plugin.db.deleteMonitoredBarrel(deadBarrel.name);

    return true;
  }
}
