package net.jasonstone.ironthing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Barrel;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

import io.prometheus.client.Gauge;

public class MonitoredBarrel {
  public Location location;
  public Block barrelBlock;
  public Barrel barrel;

  public boolean exists;

  public String name;

  public ItemStack[] items;

  public World world;

  public String toString() {
    return String.format("Monitored Barrel \"%s\": X:%d Y:%d Z:%d", name, barrelBlock.getX(), barrelBlock.getY(),
        barrelBlock.getZ());
  }

  public MonitoredBarrel(String name, String worldName, int x, int y, int z) {
    world = Bukkit.getWorld(worldName);
    location = new Location(world, x, y, z);

    this.name = name;
    barrelBlock = location.getBlock();
    exists = initMonitoredBarrel();
  }

  public boolean initMonitoredBarrel() {
    try {
      barrel = (Barrel) barrelBlock.getState();
      return true;
    } catch (ClassCastException e) {
      return false;
    }
  }

  public Map<String, Integer> getContents() {
    HashMap<String, Integer> ret = new HashMap<String, Integer>();

    if (!exists) {
      return ret;
    }

    for (ItemStack item : barrel.getInventory().getContents()) {
      if (item != null) {
        String itemName = item.getType().toString();

        Integer curr = ret.get(itemName);
        if (curr == null) {
          curr = 0;
        }

        ret.put(itemName, curr + item.getAmount());
      }
    }

    return ret;
  }

  public void createEffect() {
    world.spawnParticle(Particle.CRIT_MAGIC, location.add(0.5f, 1, 0.5f), 100);
    world.playSound(location, Sound.BLOCK_BEACON_ACTIVATE, 1, 1f);
  }

  public void deleteEffect() {
    world.spawnParticle(Particle.CRIT_MAGIC, location.add(0.5f, 1, 0.5f), 50);
    world.spawnParticle(Particle.CRIT, location.add(0.5f, 1, 0.5f), 50);
    world.playSound(location, Sound.BLOCK_BEACON_DEACTIVATE, 1, 1f);
  }

  public void ding(Gauge gauge) {
    if (!exists) {
      world.spawnParticle(Particle.WHITE_ASH, location.add(0.5f, 1, 0.5f), 1);
      return;
    }

    world.spawnParticle(Particle.SPELL_MOB, location.add(0.5f, 1, 0.5f), 5);

    Map<String, Integer> contents = getContents();

    for (String itemName : contents.keySet()) {
      gauge.labels(name, itemName).set(contents.get(itemName));
    }

  }
}
