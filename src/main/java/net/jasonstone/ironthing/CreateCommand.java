package net.jasonstone.ironthing;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CreateCommand implements CommandExecutor {
	private final IronThing plugin;

	public CreateCommand(IronThing t) {
		plugin = t;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return false;
		}

		if (args.length != 1) {
			return false;
		}

		Player player = (Player) sender;
		Block block = player.getTargetBlockExact(10);
		if (block == null) {
			return false;
		}

		if (block.getType() != Material.BARREL) {
			player.sendMessage(ChatColor.RED + "That's not a barrel. Why are you like this?.");
			return true;
		}

		MonitoredBarrel newBarrel = plugin.db.addMonitoredBarrel(args[0], block.getWorld().getName(), block.getX(),
				block.getY(), block.getZ());

		if (newBarrel == null) {
			player.sendMessage(ChatColor.RED + "FAILURE. You did something wrong.");
			player.sendMessage(
					ChatColor.RED + "Maybe this barrel is already monitored, or one already exists with that name.");
			return true;
		}

		newBarrel.createEffect();

		return true;
	}
}
